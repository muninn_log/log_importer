use crate::record::Record;
use chrono::{DateTime, NaiveDateTime, Utc};
use serde::{de, Deserialize, Deserializer};
use std::fmt;

pub struct LogRecord {
  pub time: DateTime<Utc>,
  pub level: u8,
  pub message: String,
  pub data: serde_json::Map<String, serde_json::Value>,
}

impl From<LogRecord> for Record {
  fn from(record: LogRecord) -> Self {
    Self {
      time: record.time,
      level: record.level as i16,
      message: record.message,
      data: serde_json::Value::Object(record.data),
    }
  }
}

impl<'de> Deserialize<'de> for LogRecord {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: Deserializer<'de>,
  {
    struct LogRecordVisitor;

    impl<'de> de::Visitor<'de> for LogRecordVisitor {
      type Value = LogRecord;

      fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("object")
      }

      fn visit_map<V>(self, mut map: V) -> Result<LogRecord, V::Error>
      where
        V: de::MapAccess<'de>,
      {
        let mut time = None;
        let mut level = None;
        let mut message = None;
        let mut data = serde_json::Map::new();
        while let Some(key) = map.next_key()? {
          match key {
            "time" => {
              let aasd: serde_json::Value = map.next_value()?;

              match aasd {
                serde_json::Value::String(iso_string) => {
                  time = Some(
                    DateTime::parse_from_rfc3339(&iso_string)
                      .map_err(|err| {
                        de::Error::custom(format!("Error when parsing date string: {}", err))
                      })?
                      .with_timezone(&Utc),
                  )
                }
                serde_json::Value::Number(number) => {
                  if let Some(timestamp) = number.as_i64() {
                    let secs = timestamp / 1000;
                    let nanos = (timestamp.abs() % 1000) * 1000;
                    time = Some(DateTime::from_utc(
                      NaiveDateTime::from_timestamp(secs, nanos as u32),
                      Utc,
                    ));
                  } else {
                    Err(de::Error::custom("time must be date string or timestamp"))?
                  }
                }
                _ => Err(de::Error::custom("time must be date string or timestamp"))?,
              }
            }
            "level" => {
              level = Some(map.next_value()?);
            }
            "msg" => {
              message = Some(map.next_value()?);
            }
            other => {
              data.insert(other.to_string(), map.next_value()?);
            }
          }
        }
        let time = time.ok_or_else(|| de::Error::missing_field("time"))?;
        let level = level.ok_or_else(|| de::Error::missing_field("level"))?;
        let message = message.ok_or_else(|| de::Error::missing_field("msg"))?;
        Ok(LogRecord {
          time,
          level,
          message,
          data,
        })
      }
    }

    deserializer.deserialize_map(LogRecordVisitor)
  }
}
