use chrono::{DateTime, Utc};
use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Record {
  pub time: DateTime<Utc>,
  pub level: i16,
  pub message: String,
  pub data: serde_json::Value,
}
