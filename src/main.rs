mod bunyan;
mod record;

use clap::clap_app;
use record::Record;
use reqwest::{Response, Url};
use std::{cell::Cell, error::Error, process::exit};
use tokio::sync::mpsc;
use tokio::{
  io::{stdin, AsyncBufReadExt, BufReader},
  stream::StreamExt,
  time::{self, Duration, Instant},
};

#[derive(Debug)]
struct Config {
  host: Url,
  user_agent: String,
  accept_invalid_certs: bool,

  batch_size: usize,
  batch_max_interval: usize,

  passthrough: bool,
  print_errors: bool,

  default_data: Option<serde_json::Value>,
  extra_data: Option<serde_json::Value>,
}

impl Default for Config {
  fn default() -> Self {
    Config {
      host: "http://localhost:4600".parse().unwrap(),
      user_agent: "importer".into(),
      accept_invalid_certs: false,

      batch_size: 1000,
      batch_max_interval: 5_000,

      passthrough: true,
      print_errors: true,

      default_data: None,
      extra_data: None,
    }
  }
}

#[derive(Debug)]
enum Event {
  NewRecord(Record),
  BatchInterval,
  Ended,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
  let mut config = Config::default();
  let (mut tx, mut rx) = mpsc::channel(config.batch_size + 2);

  let matches = clap_app!(importer =>
      (version: "0.1")
      (author: "Rasmus Eneman <rasmus@eneman.eu>")
      (about: "Log importer")
      (@arg HOST: -H --host +takes_value "Log server to send log records to")
      (@arg USER_AGENT: -U --("user-agent") +takes_value "User agent to use when posting longs")
      (@arg ACCEPT_INVALID_CERTS: --("accept-invalid-cert") "Skip validating TLS certificate of log server")
      (@arg BATCH_SIZE: -b --("batch-size") +takes_value "Number of log records to batch before sending them to the server (defaults to 1000)")
      (@arg BATCH_MAX_INTERVAL: -I --("batch-max")-interval +takes_value "Maximum time in milliseconds to wait for the batch to fill up before sending log records (defaults to 5 seconds)")
      (@arg NO_PASSTHROUGH: -s --("no-passthrough") "Do not passthrough stdin to stdout")
      (@arg QUIET: -q --quiet "Do not print errors with sending log records")
      (@arg DEFAULT_DATA: --("default-data") +takes_value "Default data to add to the log records (will be overwritten from data in the log records)")
      (@arg EXTRA_DATA: -d --("extra-data") +takes_value "Extra data to add to the log records (will overwrite data in the log records)")
  )
  .get_matches();

  if let Some(host) = matches.value_of("HOST") {
    config.host = host
      .parse()
      .map_err(|err| format!("Failed to parse HOST as an Uri: {}", err))?;
  }
  if let Some(user_agent) = matches.value_of("USER_AGENT") {
    config.user_agent = user_agent.to_string();
  }
  if matches.is_present("ACCEPT_INVALID_CERTS") {
    config.accept_invalid_certs = true;
  }

  if let Some(batch_size) = matches.value_of("BATCH_SIZE") {
    config.batch_size = batch_size
      .parse()
      .map_err(|err| format!("Failed to parse BATCH_SIZE as a number: {}", err))?;
  }
  if let Some(batch_max_interval) = matches.value_of("BATCH_MAX_INTERVAL") {
    config.batch_max_interval = batch_max_interval
      .parse()
      .map_err(|err| format!("Failed to parse BATCH_MAX_INTERVAL as a number: {}", err))?;
  }

  if matches.is_present("NO_PASSTHROUGH") {
    config.passthrough = false;
  }
  if matches.is_present("QUIET") {
    config.print_errors = false;
  }

  if let Some(default_data) = matches.value_of("DEFAULT_DATA") {
    config.default_data = Some(
      default_data
        .parse()
        .map_err(|err| format!("Failed to parse DEFAULT_DATA as JSON: {}", err))?,
    );
  }
  if let Some(extra_data) = matches.value_of("EXTRA_DATA") {
    config.extra_data = Some(
      extra_data
        .parse()
        .map_err(|err| format!("Failed to parse EXTRA_DATA as JSON: {}", err))?,
    );
  }

  // Batch Interval Task
  if config.batch_max_interval > 0 {
    tokio::task::spawn({
      let mut tx = tx.clone();
      let duration = Duration::from_millis(config.batch_max_interval as u64);
      let mut batch_interval = time::interval_at(Instant::now() + duration, duration);
      async move {
        loop {
          batch_interval.tick().await;
          tx.try_send(Event::BatchInterval).ok();
        }
      }
    });
  }

  // Send Task
  let send_task_handle = tokio::task::spawn({
    let print_errors = config.print_errors;
    let batch_size = config.batch_size;
    let host = config.host.clone();
    let client = reqwest::Client::builder()
      .user_agent(&config.user_agent)
      .danger_accept_invalid_certs(config.accept_invalid_certs)
      .build()?;

    async move {
      let mut buffered_records = Cell::new(Vec::with_capacity(batch_size));

      loop {
        let should_send_batch = match rx.recv().await {
          Some(Event::NewRecord(record)) => {
            buffered_records.get_mut().push(record);
            buffered_records.get_mut().len() >= batch_size
          }
          Some(Event::BatchInterval) => buffered_records.get_mut().is_empty() == false,
          Some(Event::Ended) => {
            rx.close();
            buffered_records.get_mut().is_empty() == false
          }
          Option::None => {
            if buffered_records.get_mut().len() > 0 {
              buffered_records.get_mut().is_empty() == false
            } else {
              break;
            }
          }
        };

        if should_send_batch {
          let records_to_send = buffered_records.replace(Vec::with_capacity(batch_size));
          if let Err(err) = client
            .post(host.join("/records").unwrap())
            .json(&records_to_send)
            .send()
            .await
            .and_then(Response::error_for_status)
          {
            if print_errors {
              eprintln!("Error pushing records: {}", err);
            }
          }
        }
      }
    }
  });

  // Read and Parse log loop
  {
    let default_data = config.default_data.map(|default_data| match default_data {
      serde_json::Value::Object(default_data) => default_data,
      _ => {
        eprintln!("default_data must be a JSON object");
        exit(1);
      }
    });
    let extra_data = config.extra_data.map(|extra_data| match extra_data {
      serde_json::Value::Object(extra_data) => extra_data,
      _ => {
        eprintln!("extra must be a JSON object");
        exit(1);
      }
    });
    let mut lines = BufReader::new(stdin()).lines().filter_map(Result::ok);

    while let Some(line) = lines.next().await {
      let record = serde_json::from_str::<bunyan::LogRecord>(&line);

      if let Ok(mut record) = record {
        if let Some(ref default_data) = default_data {
          let mut data = default_data.clone();
          data.append(&mut record.data);
          record.data = data;
        }
        if let Some(ref extra_data) = extra_data {
          record.data.append(&mut extra_data.clone());
        }
        tx.send(Event::NewRecord(record.into()))
          .await
          .expect("Channel should never hang up while still parsing logs");
      }

      if config.passthrough {
        println!("{}", line);
      }
    }
  }

  tx.try_send(Event::Ended).ok();
  send_task_handle.await.expect("Send task panicked");

  Ok(())
}
